package com.twuc.webApp.entities;

import com.twuc.webApp.repositories.StudentRepository;
import com.twuc.webApp.repositories.TeacherRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class OneToManyJoinColumnFromOneSIdeTest {
    @Autowired
    TeacherRepository teacherRepository;

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    EntityManager entityManager;

    @Test
    void should_save_by_join_column() {
        Student student = new Student("LiuJiajun");
        Student anotherStudent = new Student("Fancy");

        Teacher teacher = new Teacher("LiJiahao");
        teacher.addStudents(student);
        teacher.addStudents(anotherStudent);

        teacherRepository.saveAndFlush(teacher);

        entityManager.clear();

        int sutudentNumber = studentRepository.findAll().size();

        assertEquals(2, sutudentNumber);
    }
}